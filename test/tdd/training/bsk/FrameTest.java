package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	//User story 1
	public void testFrame1() throws BowlingException{
		
		int firstThrow = 2;
		int secondThrow = 4;
		
		try {
			
			Frame f = new Frame(firstThrow, secondThrow);
			assertEquals(firstThrow, f.getFirstThrow());
			assertEquals(secondThrow, f.getSecondThrow());
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
		// fail("Not yet implemented");
	}
	
	
	@Test
	//User story 2
	public void testFrame2() throws BowlingException{
		
		int firstThrow = 2;
		int secondThrow = 4;
		
		try {
			Frame f = new Frame(firstThrow, secondThrow);
			assertEquals(firstThrow + secondThrow, f.getScore());
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	@Test
	//User story 3
	public void testFrame3() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(1, 5));
			game.addFrame(new Frame(2, 5));
			game.addFrame(new Frame(1, 1));
			game.addFrame(new Frame(4, 2));
			game.addFrame(new Frame(8, 0));
			game.addFrame(new Frame(2, 3));
			game.addFrame(new Frame(1, 3));
			game.addFrame(new Frame(1, 6));
			game.addFrame(new Frame(2, 0));
			game.addFrame(new Frame(10, 0));
			
			assertEquals(new Frame(2, 5).getFirstThrow(), game.getFrameAt(2).getFirstThrow());
			assertEquals(new Frame(2, 5).getSecondThrow(), game.getFrameAt(2).getSecondThrow());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	@Test
	//User story 4
	public void testFrame4() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(1, 5));
			game.addFrame(new Frame(2, 5));
			game.addFrame(new Frame(1, 1));
			game.addFrame(new Frame(4, 2));
			game.addFrame(new Frame(8, 0));
			game.addFrame(new Frame(2, 3));
			game.addFrame(new Frame(1, 3));
			game.addFrame(new Frame(1, 6));
			game.addFrame(new Frame(2, 0));
			game.addFrame(new Frame(10, 0));
			
			int score = 0;
			
			for(Frame frame : game.getListFrame()) {
				score = score + frame.getScore();
			}
			
			assertEquals(score, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	
	@Test
	//User story 5
	public void testFrame5() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(1, 9));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(7, 2));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(4, 4));
			game.addFrame(new Frame(5, 3));
			game.addFrame(new Frame(3, 3));
			game.addFrame(new Frame(4, 5));
			game.addFrame(new Frame(8, 1));
			game.addFrame(new Frame(2, 6));
						
			assertEquals(88, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	
	@Test
	//User story 6
	public void testFrame6() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(7, 2));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(4, 4));
			game.addFrame(new Frame(5, 3));
			game.addFrame(new Frame(3, 3));
			game.addFrame(new Frame(4, 5));
			game.addFrame(new Frame(8, 1));
			game.addFrame(new Frame(2, 6));
			
						
			assertEquals(94, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	
	@Test
	//User story 7
	public void testFrame7() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(4, 6));
			game.addFrame(new Frame(7, 2));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(4, 4));
			game.addFrame(new Frame(5, 3));
			game.addFrame(new Frame(3, 3));
			game.addFrame(new Frame(4, 5));
			game.addFrame(new Frame(8, 1));
			game.addFrame(new Frame(2, 6));
			
						
			assertEquals(103, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	
	@Test
	//User story 8
	public void testFrame8() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(7, 2));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(4, 4));
			game.addFrame(new Frame(5, 3));
			game.addFrame(new Frame(3, 3));
			game.addFrame(new Frame(4, 5));
			game.addFrame(new Frame(8, 1));
			game.addFrame(new Frame(2, 6));
			
						
			assertEquals(112, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	@Test
	//User story 9
	public void testFrame9() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(8, 2));
			game.addFrame(new Frame(5, 5));
			game.addFrame(new Frame(7, 2));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(4, 4));
			game.addFrame(new Frame(5, 3));
			game.addFrame(new Frame(3, 3));
			game.addFrame(new Frame(4, 5));
			game.addFrame(new Frame(8, 1));
			game.addFrame(new Frame(2, 6));
			
						
			assertEquals(98, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	@Test
	//User story 10
	public void testFrame10() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(1, 5));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(7, 2));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(4, 4));
			game.addFrame(new Frame(5, 3));
			game.addFrame(new Frame(3, 3));
			game.addFrame(new Frame(4, 5));
			game.addFrame(new Frame(8, 1));
			game.addFrame(new Frame(2, 8));
			game.setFirstBonusThrow(7);
						
			assertEquals(90, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	
	@Test
	//User story 11
	public void testFrame11() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(1, 5));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(7, 2));
			game.addFrame(new Frame(3, 6));
			game.addFrame(new Frame(4, 4));
			game.addFrame(new Frame(5, 3));
			game.addFrame(new Frame(3, 3));
			game.addFrame(new Frame(4, 5));
			game.addFrame(new Frame(8, 1));
			game.addFrame(new Frame(10, 0));
			game.setFirstBonusThrow(7);
			game.setSecondBonusThrow(2);
						
			assertEquals(92, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
	
	
	@Test
	//User story 12
	public void testFrame12() throws BowlingException{
		
		try {
			Game game = new Game();
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.addFrame(new Frame(10, 0));
			game.setFirstBonusThrow(10);
			game.setSecondBonusThrow(10);
						
			assertEquals(300, game.calculateScore());
			
		} catch(BowlingException b) {
			System.err.println(b.getMessage());
		}
	}
}
