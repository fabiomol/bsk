package tdd.training.bsk;

import java.util.ArrayList;
import java.util.LinkedList;

public class Game {
	
	protected LinkedList<Frame> frames;
	private int firstBonusThrow, secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		this.frames = new LinkedList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		if(frames.size() < 10) {
			frames.add(frame);
		} else {
			throw new BowlingException();
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		
		return frames.get(index - 1);	
	}
	
	public LinkedList<Frame> getListFrame() {
		return frames;
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		
		for (int i = 0; i < 10; i++) {
			if(frames.get(i).isSpare() && i != 9) {
				
				frames.get(i).setBonus(frames.get(i + 1).getFirstThrow());
				
				score = score + frames.get(i).getScore();
			}
			
			else if (frames.get(i).isSpare() && i == 9) {
				frames.get(i).setBonus(firstBonusThrow);
				
				score = score + frames.get(i).getScore();
			}
			
			else if(frames.get(i).isStrike() && i < 8) {
				
				if(frames.get(i + 1).isStrike()) {
					frames.get(i).setBonus(frames.get(i + 2).getFirstThrow() + 10);
				}
				else {
					frames.get(i).setBonus(frames.get(i + 1).getFirstThrow() + frames.get(i + 1).getSecondThrow());
				}
				score = score + frames.get(i).getScore();
			}
			
			else if(frames.get(i).isStrike() && i >= 8) {
				
				frames.get(i).setBonus(firstBonusThrow + secondBonusThrow);
					
				score = score + frames.get(i).getScore();
			}
			
			else {
				score = score + frames.get(i).getScore();
			}
		}
		return score;	
	}

}
